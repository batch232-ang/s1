package com.zuitt.example;

public class Variables {
    public static void main(String[] args){
        int age;
        char middle_name;

        int x = 0;

        //int myNum = "sampleString";
        long worldPopulation = 7862881145L;
        System.out.println(worldPopulation);

        float piFloat = 3.14159f;
        System.out.println(piFloat);

        double piDouble = 3.1415926;
        System.out.println(piDouble);

        char letter = 'a';
        System.out.println(letter);

        boolean isMVP = true;
        boolean isChampion = false;
        System.out.println(isMVP);
        System.out.println(isChampion);

        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);

        String username = "theTinker23";
        System.out.println(username);

        System.out.println(username.isEmpty());
    }
}
